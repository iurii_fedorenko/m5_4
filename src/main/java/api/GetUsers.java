package api;

import io.restassured.RestAssured;
import io.restassured.response.Response;

import static io.restassured.RestAssured.given;

/**
 * Created by Iurii_Fedorenko on 8/7/2017.
 */
public class GetUsers {
    private static final String BASEURI = "https://jsonplaceholder.typicode.com";

    private void setBaseURI(){
        RestAssured.baseURI = BASEURI;
    }

    private Response getResponse(){
        setBaseURI();
        return given().get("/users").andReturn();
    }

    public int getResponseStatusCode(){
        return getResponse().getStatusCode();
    }

    public String getResponseContentType(){
        return getResponse().getContentType();
    }

    public String getResponseBody(){
        return getResponse().getBody().asString();
    }
}
