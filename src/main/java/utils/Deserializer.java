package utils;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import pojo.User;

import java.lang.reflect.Type;
import java.util.List;

public class Deserializer {
    public List<User> deserializeUsers(String response){
        Type usersListType = new TypeToken<List<User>>() {}.getType();
        List<User> list = new Gson().fromJson(response, usersListType);
        return list;
    }

}
