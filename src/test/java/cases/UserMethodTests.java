package cases;
import api.GetUsers;
import utils.Deserializer;
import pojo.User;
import org.testng.Assert;
import org.testng.annotations.Test;
import java.util.List;

public class UserMethodTests {

    @Test
    public void checkStatusCode() {
        Assert.assertEquals(new GetUsers().getResponseStatusCode(), 200);
    }

    @Test
    public void checkResponseHeader() {
        Assert.assertEquals(new GetUsers().getResponseContentType(), "application/json; charset=utf-8");
    }

    @Test
    public void checkResponseBody() {
        List<User> usersList = new Deserializer().deserializeUsers(new GetUsers().getResponseBody());
        Assert.assertEquals(usersList.size(), 10);
    }
}
